package com.image.api.model;


import com.image.api.model.request.StockImageRequest;

public class StockImageData implements ImageData {
    private long stockId;
    private String stockSymbol;
    private String imageType;
    private ImagePriority imagePriority;
    private byte[] stockImage;

    public StockImageData(long stockId, String stockSymbol, String imageType, byte[] stockImage) {
        this.stockId = stockId;
        this.stockSymbol = stockSymbol;
        this.imageType = imageType;
        this.stockImage = stockImage;
    }

    public static StockImageData from(StockImageRequest request) {
        return new StockImageData(request.getStockId(),
                request.getStockSymbol(),
                request.getImageType(),
                request.getStockImage());
    }

    public ImagePriority getImagePriority() {
        return imagePriority != null ? imagePriority : ImagePriority.BASIC;
    }

    @Override
    public String getCommonName() {
        return stockSymbol;
    }

    @Override
    public long getId() {
        return stockId;
    }

    @Override
    public byte[] getImageByteArray() {
        return stockImage;
    }

    @Override
    public String getImageType() {
        return imageType;
    }
}
