package com.image.api.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.MediaType;

@Data
@Builder
public class ImageContent {
    private MediaType imageType;
    private byte[] imageByteArray;
}
