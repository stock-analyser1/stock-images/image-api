package com.image.api.model;

public interface ImageShortNameAndLocationOnly {
    long getId();
    String getImagePath();
}
