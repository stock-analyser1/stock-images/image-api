package com.image.api.model.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StockImageRequest {
    private long stockId;
    private String stockSymbol;
    private String imageType;
    private byte[] stockImage;

    @Override
    public String toString() {
        return "StockImageRequest{" +
                "stockId=" + stockId +
                ", stockSymbol='" + stockSymbol + '\'' +
                ", imageType='" + imageType + '\'' +
                '}';
    }
}
