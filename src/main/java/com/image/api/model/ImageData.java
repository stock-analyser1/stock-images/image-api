package com.image.api.model;

public interface ImageData {
    String getCommonName();

    long getId();

    byte[] getImageByteArray();

    String getImageType();
}
