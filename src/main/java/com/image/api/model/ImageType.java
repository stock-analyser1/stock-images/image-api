package com.image.api.model;

public enum ImageType {
    IMAGE_STOCK, IMAGE_TMP
}
