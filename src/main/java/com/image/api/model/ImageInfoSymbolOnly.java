package com.image.api.model;

public interface ImageInfoSymbolOnly {
    String getShortName();
}
