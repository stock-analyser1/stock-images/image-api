package com.image.api.model;

public enum ImageFormat {
    JPEG, PNG
}
