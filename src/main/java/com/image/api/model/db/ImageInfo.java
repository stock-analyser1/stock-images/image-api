package com.image.api.model.db;


import com.image.api.model.ImagePriority;
import com.image.api.model.ImageType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.MediaType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ImageInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long modelId;
    private ImageType imageType;
    private String shortName;
    private String imagePath;
    private ImagePriority imagePriority;
    private MediaType mediaType;
    private int imageWidth;
    private int imageHeight;
    private long imageSize;
}
