package com.image.api.model.response;

import com.image.api.model.ImagePriority;
import com.image.api.model.db.ImageInfo;
import lombok.*;
import org.springframework.http.MediaType;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StockImageDataDto {
    private long id;
    private long stockId;
    private String shortName;
    private int width;
    private int height;
    private String imageSize;
    private String mediaType;
    private ImagePriority imagePriority;

    public static StockImageDataDto from(ImageInfo imageInfo) {
        return StockImageDataDto.builder()
                .id(imageInfo.getId())
                .stockId(imageInfo.getModelId())
                .shortName(imageInfo.getShortName())
                .width(imageInfo.getImageWidth())
                .height(imageInfo.getImageHeight())
                .mediaType(imageInfo.getMediaType().getType() + "/" + imageInfo.getMediaType().getSubtype())
                .imagePriority(imageInfo.getImagePriority())
                .imageSize((imageInfo.getImageSize() /1000) + " KB")
                .build();
    }
}
