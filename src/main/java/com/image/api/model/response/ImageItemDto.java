package com.image.api.model.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ImageItemDto {
    private long id;
    private int imageCount;
}
