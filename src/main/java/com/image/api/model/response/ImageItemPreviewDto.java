package com.image.api.model.response;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImageItemPreviewDto {
    private String shortName;
    private int count;
}
