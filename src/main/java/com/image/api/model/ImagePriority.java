package com.image.api.model;

public enum ImagePriority {
    LOGO, THUMBNAIL, BASIC
}
