package com.image.api.controller;

import com.image.api.model.ImagePriority;
import com.image.api.model.ImageType;
import com.image.api.model.StockImageData;
import com.image.api.model.db.ImageInfo;
import com.image.api.model.request.StockImageRequest;
import com.image.api.model.response.ImageItemDto;
import com.image.api.model.response.ImageItemPreviewDto;
import com.image.api.model.response.StockImageDataDto;
import com.image.api.service.ImageService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("stocks")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @GetMapping
    public String hello() {
        return "Hello";
    }

    @PostMapping()
    public ResponseEntity<?> SaveImage(@RequestBody StockImageRequest stockImageRequest) throws IOException {
        imageService.saveImageFrom(StockImageData.from(stockImageRequest));
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "image/{priority}/{modelId}")
    @SneakyThrows
    public ResponseEntity<BufferedImage> getModelImage(@PathVariable ImagePriority priority, @PathVariable long modelId) {
        ImageInfo imageInfo = imageService.getImageInfoByModelIdAndPriority(modelId, priority);
        return ResponseEntity.ok().contentType(imageInfo.getMediaType()).body(ImageIO.read(new File(imageInfo.getImagePath())));
    }

    @GetMapping("image/{id}")
    @SneakyThrows
    public ResponseEntity<BufferedImage> getImage(@PathVariable long id) {
        ImageInfo imageInfo = imageService.getImageInfoById(id);
        return ResponseEntity.ok().contentType(imageInfo.getMediaType()).body(ImageIO.read(new File(imageInfo.getImagePath())));
    }

    @GetMapping("items")
    public List<ImageItemPreviewDto> getImageItemsPreview() {
        return imageService.getImagePreviewsByImageType(ImageType.IMAGE_STOCK);
    }

    @GetMapping("items/{shortName}")
    public List<ImageItemDto> getImageItems(@PathVariable String shortName) {
        return imageService.getImageItemsByShortName(shortName, ImageType.IMAGE_STOCK);
    }

    @GetMapping("data/model/{modelId}")
    public List<StockImageDataDto> getImageDataByModelId(@PathVariable long modelId) {
        List<ImageInfo> stocks = imageService.getAllImageInfoByModelId(modelId);
        return stocks.stream().map(StockImageDataDto::from).collect(Collectors.toList());
    }

    @GetMapping("data/{id}")
    public StockImageDataDto getImageDataById(@PathVariable long id) {
        return StockImageDataDto.from(imageService.getImageInfoById(id));
    }
}
