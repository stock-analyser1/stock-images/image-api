package com.image.api.service;

import com.image.api.exception.ImageNotFoundException;
import com.image.api.model.ImageInfoSymbolOnly;
import com.image.api.model.ImagePriority;
import com.image.api.model.ImageType;
import com.image.api.model.StockImageData;
import com.image.api.model.db.ImageInfo;
import com.image.api.model.response.ImageItemDto;
import com.image.api.model.response.ImageItemPreviewDto;
import com.image.api.repository.ImageInfoRepository;
import com.image.api.repository.ImageRepositry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImageService {
    @Autowired
    private ImageRepositry imageRepositry;

    @Autowired
    private ImageInfoRepository imageInfoRepository;

    public void saveImageFrom(StockImageData stockImageRequest) throws IOException {
        File file = imageRepositry.saveImage(stockImageRequest, ImageType.IMAGE_STOCK);
        BufferedImage buf = ImageIO.read(file);
        ImageInfo imageInfo = ImageInfo.builder()
                .imagePath(file.getPath())
                .imageType(ImageType.IMAGE_STOCK)
                .modelId(stockImageRequest.getId())
                .shortName(stockImageRequest.getCommonName())
                .imagePriority(stockImageRequest.getImagePriority())
                .mediaType(MediaType.parseMediaType(URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(stockImageRequest.getImageByteArray()))))
                .imageWidth(buf.getWidth())
                .imageHeight(buf.getHeight())
                .imageSize(file.length())
                .build();
        imageInfoRepository.save(imageInfo);
    }

    public BufferedImage getStockImage(String symbol, long id) {
        String dirPath = imageRepositry.getStockDirPath() + symbol + "/" + id;
        List<BufferedImage> images = imageRepositry.getImagesInDirectory(dirPath);
        return images.get(0);
    }

    public ImageInfo getImageInfoById(long id) {
        return imageInfoRepository.getById(id);
    }

    public List<ImageInfo> getInfoByType(ImageType imageType) {
        return imageInfoRepository.findAllByImageType(imageType);
    }

    public List<ImageInfo> getInfosByShortName(String shortName, ImageType imageType) {
        return imageInfoRepository.findAllByShortNameAndImageType(shortName, imageType);
    }

    public List<ImageItemPreviewDto> getImagePreviewsByImageType(ImageType imageType) {
        List<ImageInfoSymbolOnly> images = imageInfoRepository.findDistinctByImageType(imageType);
        List<ImageItemPreviewDto> ret = new ArrayList<>();
        for (ImageInfoSymbolOnly image : images) {
            ImageItemPreviewDto imagePreview = ImageItemPreviewDto.builder()
                    .count(getImageItemsByShortName(image.getShortName(), imageType).size())
                    .shortName(image.getShortName())
                    .build();
            ret.add(imagePreview);
        }
        return ret;
    }

    public List<ImageInfo> getAllImageInfoByModelId(long modelId) {
        return imageInfoRepository.findAllByModelId(modelId);
    }

    public List<ImageItemDto> getImageItemsByShortName(String shortName, ImageType imageType) {
        List<Long> images = imageInfoRepository.findDistinct(shortName);
        List<ImageItemDto> ret = new ArrayList<>();
        for (long image : images) {
            ImageItemDto imageItem = ImageItemDto.builder()
                    .id(image)
                    .imageCount(imageInfoRepository.findAllByModelIdAndImageType(image, imageType).size())
                    .build();
            ret.add(imageItem);
        }
        return ret;
    }

    public ImageInfo getImageInfoByModelIdAndPriority(long imageId, ImagePriority imagePriority) {
        ImageInfo imageInfo = imageInfoRepository.findImageInfoByModelIdAndImagePriority(imageId, imagePriority);
        if (imageInfo == null) {
            throw new ImageNotFoundException();
        }
        return imageInfo;
    }

}
