package com.image.api.repository;

import com.image.api.model.ImageType;
import com.image.api.model.ImageData;
import com.image.api.model.StockImageData;
import org.springframework.stereotype.Repository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Repository
public class ImageRepositry {
    private static final String BASE_LOCATION = "store/images/";
    private static final String BASE_LOCATION_STOCKS = "store/images/stocks/";

    public File saveImage(ImageData imageData, ImageType imageModel) throws IOException {
        return switch (imageModel) {
            case IMAGE_TMP -> saveTmpImage(imageData);
            case IMAGE_STOCK -> saveStockImage((StockImageData) imageData);
        };
    }

    public String getStockDirPath() {
        return BASE_LOCATION_STOCKS;
    }

    public List<BufferedImage> getImagesInDirectory(String dirPath) {
        File file = new File(dirPath);
        File[] files = file.listFiles();
        assert files != null;
        List<BufferedImage> images = Arrays.stream(files).map(f -> {
            try {
                return ImageIO.read(f);
            } catch (IOException ignore) {

            }
            return null;
        }).toList();
        return images;
    }

    private File saveTmpImage(ImageData imageData) throws IOException {
        String name = BASE_LOCATION + "tmp/" + UUID.randomUUID();
        File file = new File(name + ".jpg");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(imageData.getImageByteArray());
        return file;
    }

    private File saveStockImage(StockImageData stockImageRequest) throws IOException {
        String directoryLocation = String.format("%s/%s/%s", BASE_LOCATION_STOCKS, stockImageRequest.getCommonName().toLowerCase(Locale.ROOT), stockImageRequest.getId());
        String name = directoryLocation + "/" + UUID.randomUUID();
        File directory = new File(directoryLocation);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String extension = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(stockImageRequest.getImageByteArray()));
        File file = new File(name + "." + extension.split("/")[1]);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(stockImageRequest.getImageByteArray());
        return file;
    }
}
