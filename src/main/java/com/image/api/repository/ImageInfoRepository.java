package com.image.api.repository;

import com.image.api.model.ImageInfoSymbolOnly;
import com.image.api.model.ImagePriority;
import com.image.api.model.ImageType;
import com.image.api.model.db.ImageInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ImageInfoRepository extends JpaRepository<ImageInfo, Long> {

    ImageInfo findImageInfoByModelIdAndImageType(long imageId, ImageType imageType);

    ImageInfo findImageInfoByModelIdAndImagePriority(long imageId, ImagePriority imagePriority);

    List<ImageInfo> findAllByImageType(ImageType imageType);

    List<ImageInfo> findAllByShortNameAndImageType(String shortName, ImageType imageType);

    @Query("SELECT DISTINCT modelId FROM ImageInfo WHERE shortName LIKE %:shortName%")
    List<Long> findDistinct(String shortName);

    List<ImageInfoSymbolOnly> findDistinctByImageType(ImageType imageType);

    List<ImageInfo> findAllByModelIdAndImageType(long modelId, ImageType imageType);

    List<ImageInfo> findAllByModelId(long modelId);

}
